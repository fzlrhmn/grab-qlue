/**
 * Created by fzlrhmn on 8/24/16.
 */
require('dotenv').config();
let key = {};

if (process.env.ENV === 'local') {
   key.key = process.env.KEY
}else {
  key.clientId = 'gme-pemprovdkidiskominfomas',
  key.clientSecret = 'VsFlGmJMjZMAcg2NHIMdc-3d9JA='
}
let googleMapsClient = require('@google/maps').createClient(key);

module.exports = function(latitude,longitude) {
    return new Promise((resolve, reject) => {
        googleMapsClient.reverseGeocode({
            latlng : [latitude,longitude]
        }, function(err, response) {
            if (err) {
              reject(err);
            }else{
              resolve(response.json.results[0].formatted_address);
            }
        });
    })
};
