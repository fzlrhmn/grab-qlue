/**
 * Created by fzlrhmn on 9/22/16.
 */

let chai = require('chai');
let assert = chai.assert;
let expect = chai.expect;
var should = require('chai').should();
let push = require('../queue/queue-push-notification');
let fs = require('fs');

describe('Push Notification Queue Module', function() {
    this.timeout(500000);

    it('should be get queued', function(done) {
        fs.readFile('./log/qlue_2016-09-21 14:50:47.json', 'utf8',(err, data) => {
            if (err) {
                done(err)
            }
            else{
                let json = JSON.parse(data);

                json.forEach(item => {
                    var heading = null;

                    if (process.env.ENV === 'local') {
                        heading = 'NOTIFIKASI DEVELOPMENT';
                    }else{
                        heading = 'Notifikasi Laporan CRM';
                    }

                    push({
                        id_report   : item.id,
                        id_skpd     : item.kode_kelurahan,
                        channel     : 'qlue',
                        heading     : heading,
                        message     : "Laporan #" + item.id + ' Baru Saja Masuk',
                        status      : 'wait'
                    });
                });

                assert.isArray(json);
                setTimeout(function() { done(); }, 500);

            }
        })
    })
});


