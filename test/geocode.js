/**
 * Created by fzlrhmn on 8/24/16.
 */
let chai = require('chai');
let assert = chai.assert;
let expect = chai.expect;
var should = require('chai').should()
let geocode = require('../geocode');

describe('Geocode Module', function() {
    this.timeout(500000);

    it('should be get geocode', function(done) {
        geocode(-6.151095,106.698647)
            .then((result) => {
                expect(result).to.be.a('string');
                done();
            })
            .catch((err) => {
                done(err);
            })
    })
});
