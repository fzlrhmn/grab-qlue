#! /usr/bin/node

// Load all package
let fetch       = require('node-fetch');
let fs          = require('fs');
let moment      = require('moment');
let openQueue   = require('amqplib').connect('amqp://localhost');
let slack		= require('./helpers/slack');

require('dotenv').config();

// fetch http://services.qlue.id/external/crop_generate_data.php and then push it into queue
fetch(process.env.QLUE_REPORT)
  .then(res => {

    // if res.status equal 200 and then return it as json, else write the error into file
    if (res.status === 200) {
      return res.json();
    }else {
      fs.writeFile(process.env.ERROR_LOG_PATH + '/error_grab_qlue_'+ moment().format("YYYY-MM-DD HH:mm:ss") +'.json',JSON.stringify(res),'utf8', (err) => {
          slack(JSON.stringify(res), (err,response) => {
              process.exit(0);
          });
        });
    }
  })
  .then(json => {
      if (typeof json === 'object') {
          fs.writeFileSync(process.env.REPORT_LOG_PATH + '/qlue_'+ moment().format("YYYY-MM-DD HH:mm:ss") +'.json',JSON.stringify(json),'utf8');

          // Open grab-qlue queue and create channel and then push each object into queue
          let q = process.env.QLUE_REPORT_QUEUE;
          openQueue
              .then(conn => {
                  return conn.createChannel();
              })
              .then(ch => {
                  return ch.assertQueue(q)
                      .then(ok => {

                          // if json is not null
                          if (json !== null) {
                              json.forEach((item) => {
                                  ch.sendToQueue(q, Buffer.from(JSON.stringify(item)))
                              });

                              // setTimeout to simulate sync process
                              setTimeout(function() { process.exit(0) }, 1000);
                          } else {
                              // TODO create new request queue
                              fs.writeFile(process.env.ERROR_LOG_PATH + '/error_json_qlue_'+ moment().format("YYYY-MM-DD HH:mm:ss") +'.txt','Empty Qlue Report','utf8', (err) => {
                                  slack('Empty Qlue Report Response', (err,response) => {
                                      process.exit(0);
                                  });
                              });
                          }
                      })
              })
              .catch(err => {
                  fs.writeFile(process.env.ERROR_LOG_PATH + '/error_push_queue_'+ moment().format("YYYY-MM-DD HH:mm:ss") +'.txt',err,'utf8', (err) => {
                      setTimeout(function() { process.exit(0) }, 1000);
                  });
              })
      } else {
          slack('Not valid JSON', (err,response) => {
              process.exit(0);
          });
      }
  })
  .catch(error => {
      fs.writeFile(process.env.ERROR_LOG_PATH + '/error_sync_qlue_'+ moment().format("YYYY-MM-DD HH:mm:ss") +'.txt',error,'utf8', (err) => {
          slack(JSON.stringify(error), (err,response) => {
              process.exit(0);
          });
      });
  });
