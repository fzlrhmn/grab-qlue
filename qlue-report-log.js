#! /usr/bin/node

// Load all package
let fetch = require('node-fetch');
let fs = require('fs');
let moment = require('moment');
let openQueue = require('amqplib').connect('amqp://localhost');
require('dotenv').config();

// fetch http://services.qlue.id/external/crop_generate_data_progress.php and then push it into queue
fetch(process.env.QLUE_REPORT_LOG)
  .then(res => {

    // if res.status equal 200 and then return it as json, else write the error into file
    if (res.status === 200) {
      return res.json();
    }else {
      fs.writeFile(process.env.ERROR_LOG_PATH + '/error_grab_qlue_log_'+ moment().format("YYYY-MM-DD HH:mm:ss") +'.json',JSON.stringify(res),'utf8', (err) => {
          process.exit(0);
      });
    }
  })
  .then(json => {
    json.reverse();

    // Open grab-qlue queue and create channel and then push each object into queue
    let q = process.env.QLUE_REPORT_LOG_QUEUE;
    openQueue
      .then(conn => {
        return conn.createChannel();
      })
      .then(ch => {
        return ch.assertQueue(q)
          .then(ok => {
            json.forEach((item) => {
              ch.sendToQueue(q, Buffer.from(JSON.stringify(item)))
            });

            // setTimeout to simulate sync process
            setTimeout(function() {
                fs.writeFileSync(process.env.REPORT_LOG_PATH + '/qlue_status_'+ moment().format("YYYY-MM-DD HH:mm:ss") +'.json',JSON.stringify(json),'utf8');
                process.exit(0);
            }, 1000);
          })
      })
      .catch(err => {
        console.log(err);
      })
  })
  .catch(error => {
      fs.writeFile(process.env.ERROR_LOG_PATH + '/error_sync_qlue_'+ moment().format("YYYY-MM-DD HH:mm:ss") +'.json',JSON.stringify(error),'utf8', (err) => {
          process.exit(0);
      });
  });
