let Slack       = require('slack-node');

module.exports = function(error, callback) {
    var slack = new Slack();
    var webhookUri = "https://hooks.slack.com/services/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";
    slack.setWebhook(webhookUri);
    slack.webhook({
        channel: "#qlue_report_log",
        username: "Qlue API Log",
        mrkdwn : true,
        text: "error",
        attachments : [
            {
                color : "danger",
                text : error
            }
        ]
    }, function(err, response) {
        if (err) {
            console.log(err);
            callback(err,null);
        }else{
            console.log(response);
            callback(null,response);
        }
    });
};