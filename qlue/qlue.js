let config  = require('../config');
let knex    = require('knex')(config);
const fs    = require('fs');

let qlue = {
  setReport : function (payload) {
      return new Promise((resolve, reject) => {
        if (payload.progress === 'process') {
          var status = 'in progres';
        }else{
          var status = payload.progress;
        }
        knex('report_location')
          .insert({
            id_report_source : payload.id,
            created_by : payload.user_id,
            username : payload.username,
            title : payload.title,
            created_date : payload.timestamp,
            category : payload.tag2,
            content : payload.description,
            id_master_source : 1,
            source_detail : 'qlue',
            status : status,
            latitude : payload.lat,
            longitude : payload.lng,
            address : payload.address,
            media_format : payload.format,
            media : payload.image_url,
            id_kecamatan : payload.kode_kecamatan,
            id_kelurahan : payload.kode_kelurahan,
            id_kota : payload.kode_kota,
            rt_rw : payload.rt_rw,
            back_count : payload.back_count,
            tag1 : payload.tag1
          })
          .then(function(results) {
            // if data.progress equals wait
            if (payload.progress === 'wait'){
              knex('report_location_status_log')
                  .insert({
                      id_report_location : payload.id,
                      id_skpd : null,
                      timestamp : payload.timestamp,
                      id_master_status : 2,
                      status : payload.progress,
                      the_geom : knex.raw('ST_geomfromtext("POINT(' + payload.lng + ' ' + payload.lat + ')")')
                  })
                  .then(function(results) {
                      resolve(results);
                  })
                  .catch(function(error) {
                      fs.writeFile('./log/error_insert_qlue_log_setReport_'+ payload.id +'.txt',error,'utf8',(err) => {
                          reject(error);
                      });
                  });
            }
            // else
            else{
                resolve(results);
            }
          })
          .catch(function(error) {
                fs.writeFile('./log/error_insert_qlue_report_'+ payload.id +'.txt',error,'utf8', (err) => {
                    reject(error);
                });
          });
      })
    },
    setQlueLog : function (payload) {
      return new Promise(function(resolve, reject) {
        if (payload.status === 'wait'){
  				var status = 2;
  			}
  			if(payload.status === 'process'){
  				var status = 3;
  			}
  			if(payload.status === 'complete'){
  				var status = 6;
  			}

        knex
          .raw('insert ignore into report_location_status_log (id_progress, id_report_location, id_skpd, timestamp, id_master_status, status, user_id_tl_qlue, comment, file) values (?,?,?,?,?,?,?,?,?)', [payload.id_progress,payload.post_id, null, payload.timestamp_progress, status, payload.status, payload.user_id_tl, payload.description_tl, payload.file])
  				.then(function(response) {
  					resolve(response);
  				})
  				.catch(error => {
                    fs.writeFile('./log/error_insert_qlue_log_setQlueLog_'+ payload.id +'.txt',error,'utf8', (err) => {
                        reject(error);
                    });
  				});
      });
    },
    setReportPrivate : function (payload) {
        return new Promise((resolve, reject) => {
            if (payload.progress === 'process') {
                var status = 'in progres';
            }else{
                var status = payload.progress;
            }
            knex('report_location_private')
                .insert({
                    id_report_source : payload.id,
                    created_by : payload.user_id,
                    username : payload.username,
                    title : payload.title,
                    created_date : payload.timestamp,
                    category : payload.tag2,
                    content : payload.description,
                    id_master_source : 1,
                    source_detail : 'qlue',
                    status : status,
                    latitude : payload.lat,
                    longitude : payload.lng,
                    address : payload.address,
                    media_format : payload.format,
                    media : payload.image_url,
                    id_kecamatan : payload.kode_kecamatan,
                    id_kelurahan : payload.kode_kelurahan,
                    id_kota : payload.kode_kota,
                    rt_rw : payload.rt_rw,
                    back_count : payload.back_count,
                    tag1 : payload.tag1
                })
                .then(function(results) {
                    // if data.progress equals wait
                    if (payload.progress === 'wait'){
                        knex('report_location_status_log')
                            .insert({
                                id_report_location : payload.id,
                                id_skpd : null,
                                timestamp : payload.timestamp,
                                id_master_status : 2,
                                status : payload.progress,
                                the_geom : knex.raw('ST_geomfromtext("POINT(' + payload.lng + ' ' + payload.lat + ')")')
                            })
                            .then(function(results) {
                                resolve(results);
                            })
                            .catch(function(error) {
                                fs.writeFile('./log/error_insert_qlue_log_private_'+ payload.id +'.txt',error,'utf8', (err) => {
                                    reject(error);
                                });
                            });
                    }
                    // else
                    else{
                        resolve(results);
                    }
                })
                .catch(function(error) {
                    fs.writeFile('./log/error_insert_qlue_report_private_'+ payload.id +'.txt',error,'utf8', (err) => {
                        reject(error);
                    });
                });
        })
    },
};

module.exports = qlue;
