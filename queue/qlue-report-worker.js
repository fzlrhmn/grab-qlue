let openQueue             = require('amqplib').connect('amqp://localhost');
let config                = require('../config');
let knex                  = require('knex')(config);
let fs                    = require('fs');
let qlue                  = require('../qlue/qlue');
let geocode               = require('../geocode');
let pushNotification      = require('./queue-push-notification');
let moment                = require('moment');
require('dotenv').config();
moment().format();
moment.locale('id');
let q                     = process.env.QLUE_REPORT_QUEUE;
console.log(q);
// open connection
openQueue
  .then(conn => {
    return conn.createChannel();
  })
  .then(ch => {
    // connect to grab-qlue queue
    return ch.assertQueue(q, {durable: true, noAck: false})
      .then(ok => {
        // consume message from queue
        return ch.consume(q, msg => {
          if (msg !== null) {
              //console.log(msg.content.toString());
              // parse msg to JSON
              let payload = JSON.parse(msg.content.toString());

              // Check if payload.id exist in report_location table
              knex('report_location')
                  .select('id_report_source as id')
                  .where('id_report_source', payload.id)
                  .then(result => {

                      // if no record in database
                      if (result.length === 0 ) {

                          // Set it into database after id checking and google geocode passed
                          qlue
                              .setReport(payload)
                              .then(result => {

                                  // Send to push-notification channel queue to send it to OneSignal notification service
                                  if ( payload.progress === 'wait' && payload.tag1 === 'Keluhan Masyarakat' || payload.progress === 'wait' && payload.tag1 === 'Surveyor Transjakarta' ) {
                                      var heading = null;

                                      if (process.env.ENV === 'local') {
                                          heading = 'NOTIFIKASI DEVELOPMENT';
                                      }else{
                                          heading = 'Notifikasi Laporan CRM';
                                      }

                                      pushNotification({
                                          id_report   : payload.id,
                                          id_skpd     : payload.kode_kelurahan,
                                          channel     : 'qlue',
                                          heading     : heading,
                                          message     : "Laporan #" + payload.id + ' Baru Saja Masuk',
                                          status      : 'wait'
                                      });

                                      // Acknowledge the message
                                      setTimeout(function() { ch.ack(msg);}, 700);
                                  }else{
                                      // Acknowledge the message
                                      ch.ack(msg);
                                  }
                              })
                              .catch(error => {
                                  fs.writeFile('./log/error_insert_qlue_report_'+ moment().format("YYYY-MM-DD HH:mm:ss") +'.json',JSON.stringify({error : error,data : payload}),'utf8', function () {
                                      ch.ack(msg);
                                  });
                              })
                      }
                      // If the report ID exist, acknowledge it
                      else {
                          ch.ack(msg);
                      }
                  })
                  .catch(error => {
                      fs.writeFile('./log/error_get_data_'+ payload.id + '_' + moment().format("YYYY-MM-DD HH:mm:ss") +'.txt',error,'utf8', (err) => {
                          ch.nack(msg);
                      });
                  });
          } else {
              fs.writeFile('./log/error_null_message_' + moment().format("YYYY-MM-DD HH:mm:ss") +'.json',JSON.parse(msg.content.toString()),'utf8', (err) => {
                  ch.ack(msg);
              });
          }
        })
      })
  })
  .catch(err => {
      console.log(err);
      fs.writeFile('./log/error_open_rabbitmq_' + moment().format("YYYY-MM-DD HH:mm:ss") +'.txt',err,'utf8', (err) => {
          ch.ack(msg);
      });
  });
