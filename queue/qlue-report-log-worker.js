let openQueue   = require('amqplib').connect('amqp://localhost');
let config      = require('../config');
let knex        = require('knex')(config);
let fs          = require('fs');
let qlue        = require('../qlue/qlue');
let geocode     = require('../geocode');
let moment                = require('moment');

require('dotenv').config();
moment().format();
moment.locale('id');

let q                     = process.env.QLUE_REPORT_LOG_QUEUE;

// open connection
openQueue
  .then(conn => {
    return conn.createChannel();
  })
  .then(ch => {
    // connect to grab-qlue queue
    return ch.assertQueue(q)
      .then(ok => {

        // consume message from queue
        return ch.consume(q, msg => {
          if (msg !== null) {

            // parse msg to JSON
            let payload = JSON.parse(msg.content.toString());

            // Check if payload.id exist in report_location table
            knex('report_location_status_log')
              .select('id')
              .where('id_progress', payload.id_progress)
              .then(result => {

                // if the report id doesn't exist
                if (result.length === 0 ) {
                  // Set it into database after id checking
                  qlue
                    .setQlueLog(payload)
                    .then(result => {
                      // Acknowledge the message
                      ch.ack(msg);
                    })
                    .catch(error => {
                        fs.writeFile('./log/error_insert_qlue_report_log_'+ moment().format("YYYY-MM-DD HH:mm:ss") +'.json',JSON.stringify({error : error,data : payload}),'utf8', function () {
                            ch.ack(msg);
                        });
                    })
                } else {
                  ch.ack(msg);
                }
              })
                .catch(error => {
                    ch.nack(msg);
                })
          }
        })
      })

  })
  .catch(err => {
    console.log(err);
  });
